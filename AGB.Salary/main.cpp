#include <iostream>
#include <string>


using namespace std;

struct employee {
	int ID;
	string Fname;
	string Lname;
	float Pay;
	int Hours;
};

int main()
{
	const int size = 5;
	employee Employee[size];

	for (int i = 0; i < size; i++)
	{
		cout << i+1 ;
		cout << "\n";
		cout << "Enter Employee ID: ";
		cin >> Employee[i].ID;
		cout << "Enter First Name: ";
		cin >> Employee[i].Fname;
		cout << "Enter Last Name: ";
		cin >> Employee[i].Lname;
		cout << "Enter Pay Rate: ";
		cin >> Employee[i].Pay;
		cout << "Enter Hours: ";
		cin >> Employee[i].Hours;
		"\n";
	}
	cout << "\nDisplaying Employee Information." << endl;
	for (int i = 0; i < size; i++)
	{
		cout << "ID: " << Employee[i].ID << endl;
		cout << "First Name: " << Employee[i].Fname << endl;
		cout << "Last Name: " << Employee[i].Lname << endl;
		cout << "Pay Rate: " << Employee[i].Pay << endl;
		cout << "Hours: " << Employee[i].Hours << endl; 
		cout << "Gross Pay: $" << Employee[i].Hours * Employee[i].Pay;
		cout << "\n";
		cout << "\n";
	}
	cout << "Total Gross Pay for all Employee's: $" << Employee[0].Hours * Employee[0].Pay + Employee[1].Hours * Employee[1].Pay + Employee[2].Hours * Employee[2].Pay + Employee[3].Hours * Employee[3].Pay + Employee[4].Hours * Employee[4].Pay;
	cout << "\n";
	cout << "\n";
};